#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 11 21:35:15 2018

@author: petervandoorn
"""

#Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#Importing the dataset
#We will use a TSV file instead of a CSV file 
#This is because there will be comma's in the text we are analizing
dataset = pd.read_csv('Restaurant_Reviews.tsv', delimiter = '\t', quoting = 3)


#Cleaning the text
import re 
import nltk
nltk.download('stopwords')
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer

#Creating the for loop that does the same cleanup procedure as we did for 1 line
corpus = []
for i in range(0, 1000):
    review = re.sub('[^a-zA-Z]', ' ', dataset['Review'][i])
    review = review.lower()
    review = review.split()
    ps = PorterStemmer()
    review = [ps.stem(word) for word in review if not word in set(stopwords.words('english'))]
    review = ' '.join(review)
    corpus.append(review)


#Creating the bag of words model
#we will create a table where each unique word is a column. 
from sklearn.feature_extraction.text import CountVectorizer
#by using the max_features we can select only the most relevant words. 
cv = CountVectorizer(max_features = 1500)
X = cv.fit_transform(corpus).toarray()
#creating the dependant valuable
y = dataset.iloc[:, 1]


# Splitting the dataset into the Training set and Test set
from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.20, random_state = 0)


#Feature scaling
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)



#Training the ML model Random forest
from sklearn.ensemble import RandomForestClassifier
classifier = RandomForestClassifier(n_estimators = 10, criterion = 'entropy', random_state = 0)
classifier.fit(X_train, y_train)

# Predicting the Test set results
y_pred = classifier.predict(X_test)

# Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)

#calculating the Performance

Accuracy = (cm[0,0] + cm [1,1])/(cm[0,0] + cm [1,1] + cm[1,0] + cm [0,1])
Precision = (cm[0,0])/(cm[0,0]+cm[1,1])
Recall = (cm[0,0])/(cm[0,0]+cm[0,1])
F1Score = (2*Precision*Recall)/(Precision + Recall)

print("The F1 score         =", F1Score)
print("The Accuracy         =", Accuracy)
print("Precision            =", Precision)
print("The Recall           =", Recall)

